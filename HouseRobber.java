class Solution {
    public int rob(int[] nums) {
          // F[i, boolean robOrNot]
    // F[i, true] = F[i-1, false] + money[i]
    // F[i, flase] = max(F[i-1, true], F[i-1, false])
    // base:
    // F[0, true] = money[0];
    // F[0, false] = 0;
    int fITrue = 0, fIFalse = 0, temp;
    for (int i = 0; i < nums.length; i++) {
        temp = Math.max(fITrue, fIFalse);
        fITrue = fIFalse + nums[i];
        fIFalse = temp;
    }
    return Math.max(fITrue, fIFalse);
}
        
}