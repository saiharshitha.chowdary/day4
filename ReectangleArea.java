class Solution {
    public int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
        int commonArea = getIntersect(ax1,ay1,ax2,ay2,bx1,by1,bx2,by2);
        
        int area1 = (ax2-ax1)*(ay2-ay1);
        int area2 = (bx2-bx1)*(by2-by1);
        
        return area1 + area2 - commonArea;
        
    }
    private int getIntersect(int ax1,int ay1,int ax2,int ay2,
                            int bx1, int by1, int bx2, int by2){
        int xIntrLft = Math.max(ax1,bx1);
        int xIntrRgt = Math.min(ax2,bx2);
        
        int yIntrBot = Math.max(ay1,by1);
        int yIntrTop = Math.min(ay2,by2);
        
        if (xIntrLft < xIntrRgt &&
            yIntrBot < yIntrTop){
            return (xIntrRgt - xIntrLft)*(yIntrTop - yIntrBot);
        }
        return 0;
    }
}
        
    