class Solution {
    public void sortColors(int[] nums) {
          int freq[]=new int[3];
        for(int num : nums) freq[num]++;
        for(int i=0,j=0;j<3;j++){
          while(freq[j]-- >0) nums[i++]=j;            
        }
        return ;
        
    }
}